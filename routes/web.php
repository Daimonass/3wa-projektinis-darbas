<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layouts.app');
// });

// Route::get('article/add', 'ManoController@articleAdd')->name('article.add');

Route::resource('articles' , 'ArticleController');
Route::get('/', 'ArticleController@index');

// Route::open(['article' => 'article.arcticlecontroller'])
// Route::post('article', 'arcticlecontroller@article')->name('route.newArticle');
Auth::routes();

Route::get('/home', 'ArticleController@index');

Route::resource('categories', 'CategoryController');

// Route::get('categories/{id}', 'CategoryController@show')->name('category.item');

// user routes
Route::get('profile', 'UserController@profile')->name('profile')->middleware('auth');
Route::put('profile', 'UserController@update')->name('profile.update')->middleware('auth');
