<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
        public function profile()
        {
            $user = \Auth::user();
            $title = 'Update profile';
            return view('auth.register', compact('user', 'title'));
        }

        public function update(Request $request)
            {
        $user = \Auth::user();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        return redirect()->route('articles.index')->with('message', [
            'text' => 'Profile updated!',
            'type' => 'success'
        ]);
    }
}
