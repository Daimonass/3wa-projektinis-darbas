<?php

namespace App\Http\Controllers;

use App\Category;
use App\Article;

use Illuminate\Http\Request;
use Session;
class CategoryController extends Controller
{

    public function __construct(){
        $this->middleware('auth.Admin')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $categories = Category::all();
        // pvz vietoj meniu controoler function daryk
        return view('category.index',[
            'categories' => $categories,
        ]);
        // return view('layouts.app', [ 'article' => $article,'categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required|max:255'
        ));
        // Category::create([
        //   'category' => $request->get('category'),
        // ]);

        $category = new Category;
        $category->name = $request->name;
        $category->save();

        Session::flash('success', 'New Category has been created');
        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
      $articles = Article::orderBy('id', 'desc')->get();
      return view('category.show', compact('category','articles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('category.form', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {    $this->validate($request, array(
              'name' => 'required|max:255'
        ));
        Category::find($category)->update($request->all());
        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return redirect()->route('categories.index');
    }
}
