<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ManoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function testas()
    {
         return view('testuoju.pavyzdys');
    }

    public function articleAdd()
        {
             return view('article.article');
        }
}
