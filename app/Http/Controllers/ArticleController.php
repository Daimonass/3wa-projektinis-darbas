<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function __construct(){
      $this->middleware('auth.Admin')->except(['index','show']);

      $this->middleware('auth')->except(['index']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //  $articles = Article::orderBy('id', 'desc')->paginate(9);
        //  return view('article.index' , compact('articles'));

         $search = \Request::get('search');
         $articles = Article::where('title', 'like', '%'.$search.'%')->orderBy('id','desc')->paginate(9);
         return view('article.index' , compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('article.form', compact('categories'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $article   = new Article ;
        // $article ->title = $request->title;
        // $Article ->image = $request->image;
        // $Article ->article = $request->article;
        // $article->author = 'Saulius';
        // $article->image = 'dasdasdas';
        // $article->article = 'dasdasdas';
        // $article ->save();
        // return $article->all();

        $this->validate($request, [

          'title' => 'required|max:255',
          'img_url' => 'required|image',
          'mini_description' => 'required|string|max:255',
          'description' => 'required|string',
          'category_id' => 'required|exists:categories,id',
       ]);

        if ($request->hasFile('img_url')) {
            $file = $request->file('img_url');
            $file->store('public'); // Panaudoti php artisan storage:link kad susieti public ir storage folderius.
            $request->img_url = $file->hashName();
        }

        Article::create([
          'title' => $request->get('title'),
        //   'img_url' => $request->get('img_url'),
          'img_url' => $request->img_url,
          'mini_description' => $request->get('mini_description'),
          'description' => $request->get('description'),
          'category_id' => $request->get('category_id'),
        ]);

        return redirect()->route('articles.index')->with('message', [
        'text' => 'Naujiena sukurta ivykdytas',
        'type' => 'success'
        ]);
      }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //  return view('article.show');
        $article = Article::find($id);
        return view('article.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        $categories = Category::all();
        return view('article.form', compact('article', 'categories' ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        // Article::find($article)->update($request->all());

        $this->validate($request, [

          'title' => 'required|max:255',
          'img_url' => 'image',
          'mini_description' => 'required|string|max:255',
          'description' => 'required|string',
       ]);

        $article->title =  $request->get('title');
        $article->mini_description = $request->get('mini_description');
        // $article->category_id = $request->get('category_id');
        // $article->max = $request->get('description');

        if($request->get('description')){
          $article->description = $request->get('description');
        }
        if($request->get('category_id')){
          $article->description = $request->get('category_id');
        }

        if ($request->hasFile('img_url')) {
            $file = $request->file('img_url');
            $file->store('public');
            $article->img_url = $file->hashName();
          }

          $article->save();
          return redirect()->route('articles.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();

        return redirect()->route('articles.index');
    }
}
