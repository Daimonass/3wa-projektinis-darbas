<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
    'title',
    'img_url',
    'mini_description',
    'description',
    'category_id',
    ];
     public function category()
     {
         return $this->belongsTo('App\Category');
     }
}
