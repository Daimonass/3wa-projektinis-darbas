@extends('layouts.app')

@section('content')

  @foreach ($articles->chunk(3) as $chunk)
    <div class="row">
      @foreach ($chunk as $article)
          <div class="col-md-4 newimg">
              <a href="{{ route('articles.show', $article->id) }}">

                  
                    <img class="img-responsive text-center" src="{{ asset('/storage/' . $article->img_url) }}" alt="{{ $article->title }}">

                  <h3>{{ $article->title }}</h3>
              </a>
              <p>{{ $article->mini_description }}</p>
          </div>
        @endforeach
      </div>
    @endforeach
<div class="text-center">
    {!! $articles->links(); !!}
</div>
@endsection
