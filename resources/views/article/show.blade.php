@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-md-12 naujiena">
            <h1 class="text-center">{{ $article->title}}</h1>
            <img class="img-responsive text-center" src="{{ asset('/storage/' . $article->img_url) }}" alt="{{ $article->title }}">
            <div class="naujiena">
                <p>{!! $article->description !!}</p>
                <hr/>
                <p>Kategorija: {{ $article->category->name }}</p>
             </div>

          </div>
     </div>
@if(Auth::user() && Auth::user()->isAdmin())
       <a href="{{ route('articles.edit', $article->id) }}" class="btn btn-primary">Edit</a>
     {!! Form::open(['route' => ['articles.destroy', $article->id], 'method' => 'delete', 'class' => 'pull-right','onclick'=>"return myFunction()"])  !!}
     {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
     {!! Form::close() !!}

@endif
<div id="disqus_thread"></div>
<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
  (function() { // DON'T EDIT BELOW THIS LINE
    var d = document, s = d.createElement('script');
      s.src = 'https://info-4.disqus.com/embed.js';
      s.setAttribute('data-timestamp', +new Date());
      (d.head || d.body).appendChild(s);
  })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>





@endsection
