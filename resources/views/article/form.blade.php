@extends('layouts.app')

@section('content')


    <div class="row">
    	<div class="col-md-6  col-md-offset-3">

        @if(isset($article))
            {{-- Editinimo forma --}}
          {!! Form::model($article, ['route' => ['articles.update', $article->id], 'method' => 'put', 'files' => true])  !!}
        @else
            {{-- Naujai sukurti forma --}}
          {!! Form::open(['route' => ['articles.store'], 'method' => 'post', 'files' => true])  !!}
        @endif

{{-- <div class="form-group">
    {!! Form::label('category_id', 'Categorija');!!}
	{!! Form::select('category_id', $article, null, ['class' =>'form-control']) !!}
</div> --}}





    			<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    				{!! Form::label('title', 'Title');!!}
            {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Enter title']);!!}

            @if ($errors->has('title'))
              <span class="help-block">
                  <strong>{{ $errors->first('title') }}</strong>
              </span>
            @endif

    			</div>

          <div class="form-group{{ $errors->has('mini_description') ? ' has-error' : '' }}">
    				{!! Form::label('mini_description', 'Mini description');!!}
            {!! Form::text('mini_description', null, ['class' => 'form-control', 'placeholder' => 'Enter title']);!!}

            @if ($errors->has('mini_description'))
              <span class="help-block">
                  <strong>{{ $errors->first('mini_description') }}</strong>
              </span>
            @endif

    			</div>

    			<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
            {!! Form::label('description', 'Description');!!}
    				{!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Enter description']);!!}

            @if ($errors->has('description'))
              <span class="help-block">
                  <strong>{{ $errors->first('description') }}</strong>
              </span>
            @endif

    			</div>

    			<div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
              {!! Form::label('category_id', 'Category: ');!!}

              <select class="selectpicker form-control" name="category_id" title="Pasirink ketegorija">
                  @foreach ($categories as $category)
                      <option value="{{ $category->id }}">{{$category->name}}</option>
                  @endforeach
              </select>

              @if ($errors->has('category_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('category_id') }}</strong>
                </span>
              @endif

    			</div>

                {{-- <div class="form-group">
    			    {!! Form::label('img_url', 'Image');!!}
                    {!! Form::text('img_url', null, ['class' => 'form-control', 'placeholder' => 'Image url']);!!}
    			</div> --}}

    			<div class="form-group{{ $errors->has('img_url') ? ' has-error' : '' }}">
            {!! Form::label('img_url', 'Upload image');!!}
            {!! Form::file('img_url');!!}

            @if ($errors->has('img_url'))
              <span class="help-block">
                  <strong>{{ $errors->first('img_url') }}</strong>
              </span>
            @endif

    			</div>

        {!! Form::submit('Submit', ['class' => 'btn btn-primary','onclick'=>"this.disabled=true;this.form.submit();" ]); !!}
    		{!! Form::close() !!}
    	</div>
    </div>


@endsection
