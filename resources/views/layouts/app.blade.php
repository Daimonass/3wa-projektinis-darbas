<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title', config('app.name', 'Laravel'))</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="style/login.css" /> -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

</head>

<body>
    <!--***************************** NAVIGACIJA **********************************-->
    <div class="navbackground">
        <div class="container-fluid">
            <nav class="navbar navbar-inverse" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                       <span class="sr-only">Toggle navigation</span>
                       <span class="icon-bar"></span>
                       <span class="icon-bar"></span>
                       <span class="icon-bar"></span>
                   </button>

                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="{{ route('articles.index') }}">Naujienos</a></li>
                            <li class="dropdown">
                                <a href="{{   route('categories.index')  }}" class="dropdown-toggle" data-toggle="dropdown">Kategorijos <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    @foreach (\App\Category::all() as $category)
                                        <li><a href="{{route('categories.show', $category->id)}}">{{$category->name}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>


                            <!-- Right Side Of Navbar -->
                         <ul class="nav navbar-nav navbar-right">
                           <div class="navbar-left">
                               {!! Form::open(['url' => 'articles', 'method' => 'GET', 'class' => 'navbar-form', 'role' => 'search']) !!}
                               {{-- <form class="navbar-form" role="search"> --}}
                                   <div class="input-group">
                                       <input type="text" class="form-control" placeholder="Search" name="search" id="search">
                                         <div class="input-group-btn">
                                             <button class=" btn btn-default searchbg" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                         </div>
                                   </div>
                               {!! Form::close() !!}
                           </div>
                             <!-- Authentication Links -->
                             @if (Auth::guest())
                                 <li><a href="{{ route('login') }}">Login</a></li>
                                 <li><a href="{{ route('register') }}">Register</a></li>
                             @else
                                 <li class="dropdown">
                                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                         {{ Auth::user()->name }} <span class="caret"></span>
                                     </a>

                                     <ul class="dropdown-menu" role="menu">
                                         <li>
                                             <a href="{{ route('logout') }}"
                                             onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                             Logout
                                         </a>
                                          <a href="{{ route('profile') }}">Profile</a>
                                         @if(Auth::user() && Auth::user()->isAdmin())
                                         <a href="{{ route('articles.create') }}">Create Articles</a>
                                         <a href="{{ route('categories.create') }}">Create Category</a>
                                        @endif
                                             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                 {{ csrf_field() }}
                                             </form>
                                           </li>
                                         </ul>
                                     </li>
                                 @endif
                            </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>


    <!--***************************** NAVIGACIJA END **********************************-->


    <!--***************************** Logo **********************************-->


    <div class="logo">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="col-md-3">
                        <a href="{{ route('articles.index') }}">
                           <img src="{{ asset('img/logo.png') }}" alt="logo" />
                        </a>
                    </div>
                    <div class="col-md-9  col-xs-12">
                        <div class="text-center center-block ">
                            <!-- <div class="col-md-3">
                            <p>Sekite:</p>
                        </div> -->

                            <!-- <ul class="social-links">
                                <li><a rel="nofollow" class="social-tooltip text-center" title="Google+" href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a rel="nofollow" class="social-tooltip text-center" title="Facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a rel="nofollow" class="social-tooltip text-center" title="Email" href="#"><i class="fa fa-envelope"></i></a></li>
                                <li><a rel="nofollow" class="social-tooltip text-center" title="RSS" href="#"><i class="fa fa-rss"></i></a></li>
                            </ul> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--***************************** LOGO END **********************************-->
<div class="container main">
@yield('content')
</div>



    <!-- Scripts -->

    <!-- <script src="{{ asset('js/app.js') }}"></script> -->
    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="https://use.fontawesome.com/683d477594.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="{{ asset('js/confirm.js') }}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
      var editor_config = {
        path_absolute : "/",
        selector: "textarea",
        plugins: [
          "advlist autolink lists link image charmap print preview hr anchor pagebreak",
          "searchreplace wordcount visualblocks visualchars code fullscreen",
          "insertdatetime media nonbreaking save table contextmenu directionality",
          "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
          var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
          var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

          var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
          if (type == 'image') {
            cmsURL = cmsURL + "&type=Images";
          } else {
            cmsURL = cmsURL + "&type=Files";
          }

          tinyMCE.activeEditor.windowManager.open({
            file : cmsURL,
            title : 'Filemanager',
            width : x * 0.8,
            height : y * 0.8,
            resizable : "yes",
            close_previous : "no"
          });
        }
      };

      tinymce.init(editor_config);
    </script>
    @yield('scripts')

</body>

</html>
