@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h1>Kategorijos</h1>
            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($categories as $category)
                        <tr>
                            <td><a href="{{route('categories.show', $category->id)}}">{{$category->name}}</a>
                              @if(Auth::user() && Auth::user()->isAdmin())
                                <a class="btn btn-primary pull-right" href="{{ route('categories.edit', $category->id)}}"/> Edit </a>
                                {!! Form::open(['route' => ['categories.destroy', $category->id], 'method' => 'delete', 'class' => ' pull-right', 'onclick'=>"return myFunction()"])  !!}
                                {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
                                {!! Form::close() !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
