@extends('layouts.app')

@section('content')

    @if(isset($category))
        {{-- Editinimo forma --}}
      {!! Form::model($category, ['route' => ['categories.update', $category->id], 'method' => 'put'])  !!}
    @else
        {{-- Naujai sukurti forma --}}
      {!! Form::open(['route' => ['categories.store'], 'method' => 'post'])  !!}
    @endif

    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'Sukurti kategorija');!!}
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter category']);!!}
       @if ($errors->has('name'))
         <span class="help-block">
             <strong>{{ $errors->first('name') }}</strong>
         </span>
       @endif
    </div>
    {!! Form::submit('Submit', ['class' => 'btn btn-primary', 'onclick'=>"this.disabled=true;this.form.submit();"]); !!}
{!! Form::close() !!}

@endsection
