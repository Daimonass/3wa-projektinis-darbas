<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('App\Article');

        for ($i=0; $i < 10 ; $i++) {
            DB::table('articles')->insert([
                'title' => $faker->name,
                'img_url' => $faker->imageUrl($width = 400, $height = 400),
                'mini_description' => $faker->text($maxNbChars = 180) ,
                'description' => $faker->text($maxNbChars = 1500),
                'category_id' => $faker->numberBetween(1,5)
            ]);
        }
    }
}
